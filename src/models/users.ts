const { Entity, Column, PrimaryGeneratedColumn, UpdateDateColumn, CreateDateColumn }  = require("typeorm");
const bcrypts = require('bcryptjs');

@Entity('users')
export default class Users {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column()
  last_name: string;

  @Column()
  email: string;

  @Column()
  password: string;

  @Column()
  phone_number: string;

  @Column()
  char_name: string;

  @Column()
  post_code: string;

  @Column()
  street: string;

  @Column()
  street_number: string;

  @Column()
  district: string;

  @Column()
  city: string;

  @Column()
  state: string;

  @Column()
  country: string;

  @Column()
  document_id: string;

  @Column()
  birthday: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  hashPassword() {
    this.password = bcrypts.hashSync(this.password, 8);
  }

  checkIfUnencryptedPasswordIsValid(unencryptedPassword: string) {
    return bcrypts.compareSync(unencryptedPassword, this.password);
  }

}

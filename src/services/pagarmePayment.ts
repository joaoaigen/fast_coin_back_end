import {Request, Response} from "express";
import Axios from "axios";
import config from "../config/configs";
import Users from "../models/users";
import {getRepository} from "typeorm";

const baseUrl = 'https://api.pagar.me/1/';

interface Transaction {
    amount: number;
    card_holder_name: string;
    card_expiration_date: number;
    card_number: number;
    card_cvv: number;
    payment_method: string;
}

export default {

    async createTransaction(request: Request, response: Response) {
        const {userId, username: email} = response.locals.jwtPayload;

        const user = getRepository(Users);
        const dataUser = await user.findOne(userId, {
            select: [
                'id',
                'name',
                'last_name',

            ]
        });

        return response.json(dataUser);

        await Axios.post(baseUrl + 'transactions/?api_key=' + config.pagarme_api_key_test, {
            amount: '1000',
            card_holder_name: 'Brenda Ferrareto',
            card_expiration_date: '1027',
            card_number: '5502094423775985',
            card_cvv: '499',
            payment_method: 'credit_card',
            customer: {
                "external_id": "#3311",
                "name": "Joao Paulo Nogueira Santos",
                "type": "individual",
                "country": "br",
                "email": "newemo14@gmail.com",
                "documents": [
                    {
                        "type": "cpf",
                        "number": "43903226807"
                    }
                ],
                "phone_numbers": ["+5519983573514"],
                "birthday": "1997-04-14"
            },
            billing: {
                'name': 'Brenda Sthepanie Ferenzini Ferrareto',
                'address': {
                    'country': 'br',
                    'street': 'Frederico pollo, 265',
                    'street_number': '787',
                    'state': 'sp',
                    'city': 'Americana',
                    'neighborhood': 'Vila Massucheto',
                    'zipcode': '13465580'
                }
            },
            shipping: {
                'name': 'Joao Paulo Nogueira Santos',
                'fee': 1020,
                'delivery_date': '2018-09-22',
                'expedited': false,
                'address': {
                    'country': 'br',
                    'street': 'Avenida Brigadeiro Faria Lima',
                    'street_number': '1811',
                    'state': 'sp',
                    'city': 'Sao Paulo',
                    'neighborhood': 'Jardim Paulistano',
                    'zipcode': '01451001'
                }
            },
            'items': [
                {
                    "id": "r123",
                    "title": "Red pill",
                    "unit_price": 10000,
                    "quantity": 1,
                    "tangible": true
                }
            ],
            metadata: {
                userId: userId
            }
        }).then(result => {
            const {characterName} = request.body;
            const {status, object, amount, id, metadata} = result.data;

            return response.json({
                status,
                object,
                amount,
                id,
                metadata
            });
        });
    },

    async consultTransaction(request: Request, response: Response) {
        const {id} = request.params
    }
}

import {Request, Response} from "express";
import configs from '../config/configs';
import axios from "axios";
import qs from 'qs';

var base64encodedData = Buffer.from(configs.paypal_client_id + ':' + configs.paypal_secret).toString('base64');
const url = 'https://api.sandbox.paypal.com/v2';
const token = '';
const headers = {
    headers: {
        'Accept': 'application/json',
        'Accept-Language': 'en_US',
        'Authorization': 'Bearer A21AAIt-HBXuob9ntb1u3yRQPU2eK46ebs12Ogsr8ESCAV1VrnUpQOyRK-nAnHgkB0GehfxVYzpCCe1t9_zz8915Gp9F-utUQ'//`Basic ${base64encodedData}`,
    }
}

export default {

    async getToken(request: Request, response: Response) {

        await axios.post('https://api.sandbox.paypal.com/v1/oauth2/token', qs.stringify({
            'grant_type': 'client_credentials'
        }), headers).then((resp) => {
            return resp.data.access_token;
        }).catch((error) => {
            return response.json(error);
        })

    },

    async teste(request: Request, response: Response) {
        await axios.post('https://api.sandbox.paypal.com/v2/checkout/orders', {
            "intent": "CAPTURE",
            "purchase_units": [
                {
                    "amount": {
                        "currency_code": "USD",
                        "value": "100.00"
                    }
                }
            ]
        }, headers).then((resp) => {
            return response.json(resp.data);
        }).catch((error) => {
            return response.json(error);
        })
    }
}

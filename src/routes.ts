import { Router } from 'express';
//import multer from "multer";

//SERVICES
import Pagarme from './services/pagarmePayment';

//CONTROLLERS
import AuthController from "./controllers/authController";
import UsersController from "./controllers/usersController"

//MIDDLEWARE
import { authJWT } from "./middleware/authJWT";

const routes = Router();
//const upload = multer(uploadConfig);

routes.post('/login', AuthController.login);
routes.post('/register', UsersController.registerUser);

routes.post('/change-password', authJWT, AuthController.changePassword);
//routes.post('/orphanages', authJWT, upload.array('images'), OrphanagesController.create);

//routes.get('/paypal/token', Services.getToken);
routes.get('/pagarme/createTransaction', authJWT, Pagarme.createTransaction);
routes.get('/pagarme/consultTransaction', Pagarme.consultTransaction);

export default routes;
